# based on https://github.com/LLRCMS/KLUBAnalysis/blob/VBF_legacy/src/PuJetIdSF.cc

import os
from copy import deepcopy as copy

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from analysis_tools.utils import import_root, getContentHisto2D
from Base.Modules.baseModules import JetLepMetModule, DummyModule, JetLepMetSyst

import correctionlib
correctionlib.register_pyroot_binding()

ROOT = import_root()


class btag_SFRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(btag_SFRDFProducer, self).__init__(*args, **kwargs)
        self.year = kwargs.pop("year")
        self.isMC = kwargs.pop("isMC")
        self.isUL = kwargs.pop("isUL")
        self.lep_pt = kwargs.pop("lep_pt", "{}")
        self.lep_eta = kwargs.pop("lep_eta", "{}")
        self.lep_phi = kwargs.pop("lep_phi", "{}")
        self.lep_mass = kwargs.pop("lep_mass", "{}")
        # self.incl_uncertainties = kwargs.pop("incl_uncertainties", ["central"])
        # self.comb_uncertainties = kwargs.pop("comb_uncertainties", ["central"])
        self.reshape_uncertainties = kwargs.pop("reshape_uncertainties", ["central"])

        if self.isMC and self.isUL:
            if not os.getenv("_btag_SF"):
                os.environ["_btag_SF"] = "_btag_SF"
                base = "{}/{}/src/Corrections/BTV".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                if "/libCorrectionsBTV.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsBTV.so")
                ROOT.gROOT.ProcessLine(".L {}/interface/btag_SFinterface.h".format(base))
                json_path = ("/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/"
                    "POG/BTV/{0}/btagging.json.gz")
                if self.year == 2018:
                    filename = json_path.format("2018_UL")
                else:
                    raise ValueError("2016 and 2017 not yet implemented")

                ROOT.gInterpreter.Declare("""
                    auto _btag_SF = btag_SFinterface(%s, "%s");
                """ % (int(self.year), filename))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    std::vector<double> get_btag_sf (
                        Vfloat Jet_pt, Vfloat Jet_eta, Vfloat Jet_phi, Vfloat Jet_mass,
                        Vint Jet_puId, Vfloat Jet_jetId,
                        Vint Jet_hadronFlavour, Vfloat Jet_btagDeepFlavB,
                        int pairType, int dau1_index, int dau2_index,
                        Vfloat muon_pt, Vfloat muon_eta, Vfloat muon_phi, Vfloat muon_mass,
                        Vfloat electron_pt, Vfloat electron_eta,
                        Vfloat electron_phi, Vfloat electron_mass,
                        Vfloat tau_pt, Vfloat tau_eta, Vfloat tau_phi, Vfloat tau_mass,
                        std::string incl_uncertainty, std::string comb_uncertainty,
                        std::string reshape_uncertainty
                    )
                    {
                        float dau1_pt, dau1_eta, dau1_phi, dau1_mass;
                        float dau2_pt, dau2_eta, dau2_phi, dau2_mass;
                        if (pairType == 0) {
                            dau1_pt = muon_pt.at(dau1_index);
                            dau1_eta = muon_eta.at(dau1_index);
                            dau1_phi = muon_phi.at(dau1_index);
                            dau1_mass = muon_mass.at(dau1_index);
                        } else if (pairType == 1) {
                            dau1_pt = electron_pt.at(dau1_index);
                            dau1_eta = electron_eta.at(dau1_index);
                            dau1_phi = electron_phi.at(dau1_index);
                            dau1_mass = electron_mass.at(dau1_index);
                        } else if (pairType == 2) {
                            dau1_pt = tau_pt.at(dau1_index);
                            dau1_eta = tau_eta.at(dau1_index);
                            dau1_phi = tau_phi.at(dau1_index);
                            dau1_mass = tau_mass.at(dau1_index);
                        } else {
                            dau1_pt = -999.;
                            dau1_eta = -999.;
                            dau1_phi = -999.;
                            dau1_mass = -999.;
                        }
                        dau2_pt = tau_pt.at(dau2_index);
                        dau2_eta = tau_eta.at(dau2_index);
                        dau2_phi = tau_phi.at(dau2_index);
                        dau2_mass = tau_mass.at(dau2_index);

                        return _btag_SF.get_btag_sf(
                        Jet_pt, Jet_eta, Jet_phi, Jet_mass, Jet_jetId, Jet_puId,
                        Jet_hadronFlavour, Jet_btagDeepFlavB,
                        {dau1_pt, dau2_pt}, {dau1_eta, dau2_eta},
                        {dau1_phi, dau2_phi}, {dau1_mass, dau2_mass}, incl_uncertainty,
                        comb_uncertainty, reshape_uncertainty);
                    }
                """)

    def run(self, df):
        branches_to_save = []
        if not self.isMC or not self.isUL:
            return df, branches_to_save
        # branches = ['bTagweightL{}', 'bTagweightM{}', 'bTagweightT{}', 'bTagweightReshape{}']
        branch = 'bTagweightReshape{}'
        for uncertainty in self.reshape_uncertainties:
            postfix = ("_" + uncertainty) if uncertainty != "central" else ""
            df = df.Define("btagweight_results%s" % postfix, "_btag_SF.get_btag_sf("
               "Jet_pt{0}, Jet_eta, Jet_phi, Jet_mass{0}, Jet_jetId, Jet_puId, "
               "Jet_hadronFlavour, Jet_btagDeepFlavB, "
               "{1}, {2}, {3}, {4}, "
               "\"central\", \"central\", \"{5}\""
               ")".format(self.jet_syst, self.lep_pt, self.lep_eta, self.lep_phi, self.lep_mass,
                    uncertainty))
            # for ib, branch in enumerate(branches):
                # df = df.Define(branch.format(postfix), "btagweight_results%s[%s]" % (postfix, ib))
            df = df.Define(branch.format(postfix), "btagweight_results%s[3]" % (postfix))
            branches_to_save.append(branch.format(postfix))
        return df, branches_to_save


def btag_SFRDF(**kwargs):
    """
    Module to obtain btagging deepJet SFs with their uncertainties.

    Required RDFModules: :ref:`HHLepton_HHLeptonRDF`.
    
    :param reshape_uncertainties: name of the systematic to consider among ``central`` (default),
        ``down_cferr1``, ``down_cferr2``, ``down_hf``, ``down_hfstats1``, ``down_hfstats2``, ``down_jes``,
        ``down_jesAbsoluteMPFBias``, ``down_jesAbsoluteScale``, ``down_jesAbsoluteStat``, ``down_jesFlavorQCD``,
        ``down_jesFragmentation``, ``down_jesPileUpDataMC``, ``down_jesPileUpPtBB``, ``down_jesPileUpPtEC1``,
        ``down_jesPileUpPtEC2``, ``down_jesPileUpPtHF``, ``down_jesPileUpPtRef``, ``down_jesRelativeBal``,
        ``down_jesRelativeFSR``, ``down_jesRelativeJEREC1``, ``down_jesRelativeJEREC2``, ``down_jesRelativeJERHF``,
        ``down_jesRelativePtBB``, ``down_jesRelativePtEC1``, ``down_jesRelativePtEC2``, ``down_jesRelativePtHF``,
        ``down_jesRelativeSample``, ``down_jesRelativeStatEC``, ``down_jesRelativeStatFSR``,
        ``down_jesRelativeStatHF``, ``down_jesSinglePionECAL``, ``down_jesSinglePionHCAL``, ``down_jesTimePtEta``,
        ``down_lf``, ``down_lfstats1``, ``down_lfstats2``, ``up_cferr1``, ``up_cferr2``, ``up_hf``, ``up_hfstats1``,
        ``up_hfstats2``, ``up_jes``, ``up_jesAbsoluteMPFBias``, ``up_jesAbsoluteScale``, ``up_jesAbsoluteStat``,
        ``up_jesFlavorQCD``, ``up_jesFragmentation``, ``up_jesPileUpDataMC``, ``up_jesPileUpPtBB``,
        ``up_jesPileUpPtEC1``, ``up_jesPileUpPtEC2``, ``up_jesPileUpPtHF``, ``up_jesPileUpPtRef``,
        ``up_jesRelativeBal``, ``up_jesRelativeFSR``, ``up_jesRelativeJEREC1``, ``up_jesRelativeJEREC2``,
        ``up_jesRelativeJERHF``, ``up_jesRelativePtBB``, ``up_jesRelativePtEC1``, ``up_jesRelativePtEC2``,
        ``up_jesRelativePtHF``, ``up_jesRelativeSample``, ``up_jesRelativeStatEC``, ``up_jesRelativeStatFSR``,
        ``up_jesRelativeStatHF``, ``up_jesSinglePionECAL``, ``up_jesSinglePionHCAL``, ``up_jesTimePtEta``, ``up_lf``,
        ``up_lfstats1``, ``up_lfstats2``.
    :type reshape_uncertainties: list of str

    :param lep_pt: pt of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_pt, lep2_pt}"). Default: none.
    :type lep_pt: str

    :param lep_eta: eta of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_eta, lep2_eta}"). Default: none.
    :type lep_eta: str

    :param lep_phi: phi of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_phi, lep2_phi}"). Default: none.
    :type lep_phi: str

    :param lep_mass: mass of the leptons to remove from the Jet collection. Can be included as a
        straight vector from the RDataFrame or as vector made out of floats available in the
        RDataFrame (e.g. "{lep1_mass, lep2_mass}"). Default: none.
    :type lep_mass: str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: btag_SFRDF
            path: Corrections.BTV.btag_SF
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                isUL: self.dataset.has_tag('ul')
                reshape_uncertainties: [central, ...]
                lep_pt: ...
                lep_eta: ...
                lep_phi: ...
                lep_mass: ...

    """
    return lambda: btag_SFRDFProducer(**kwargs)

