import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.BTV.btagCorrections import btagSFRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"
get_test_file = lambda x: os.path.expandvars(os.path.join(test_folder, x))

import pandas as pd

import argparse
parser = argparse.ArgumentParser(description='Plotter options')
parser.add_argument('-s','--save', action='store_true', default=False,
    help="Stores test results in a pickle file")
parser.add_argument('-c','--check', action='store_true', default=False,
    help="Compares test results with previously stored results")
options = parser.parse_args()

def btag_test(df, year, runPeriod, btag_algo, wps=["shape", "L"], print_test=True):
    btagsf = btagSFRDF(
        isMC=True,
        year=year,
        runPeriod=runPeriod,
        btag_algo=btag_algo,
        wps=wps,
        jet_syst=""
    )()
    df, _ = btagsf.run(df)
    if print_test:
        h1 = df.Histo1D("btagsf_shape")
        h2 = df.Histo1D("btagsf_wpL")
        print(f"btag {year}{runPeriod} shape SF Integral: %s, Mean: %s, Std: %s" % (
            h1.Integral(), h1.GetMean(), h1.GetStdDev()))
        print(f"btag {year}{runPeriod} wp L SF Integral: %s, Mean: %s, Std: %s" % (
            h2.Integral(), h2.GetMean(), h2.GetStdDev()))
    return df, (h1.Integral(), h1.GetMean(), h1.GetStdDev())

if __name__ == "__main__":
    results = []

    df_2022 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022.root"))
    _, res = btag_test(df_2022, 2022, "preEE", "PNetB")
    results.append(("2022", res[0], res[1], res[2]))

    df_2022_postEE = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022_postee.root"))
    _, res = btag_test(df_2022_postEE, 2022, "postEE", "PNetB")
    results.append(("2022_postEE", res[0], res[1], res[2]))

    df_2023 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023.root"))
    _, res = btag_test(df_2023, 2023, "preBPix", "PNetB")
    results.append(("2023", res[0], res[1], res[2]))

    df_2023_postBPix = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023_postbpix.root"))
    _, res = btag_test(df_2023_postBPix, 2023, "postBPix", "PNetB")
    results.append(("2023_postBPix", res[0], res[1], res[2]))

    pd_df = pd.DataFrame(results)

    results_path = os.path.expandvars("$CMSSW_BASE/src/Corrections/BTV/scripts/results.pickle")
    if options.save:
        pd_df.to_pickle(results_path)
    elif options.check:
        stored_df = pd.read_pickle(results_path)
        pd_matches_with_saved_df = pd_df.equals(stored_df)
        if not pd_matches_with_saved_df:
            raise ValueError("Test results obtained do not agree with the stored results. If this "
                "is expected, please execute locally python3 test.py -s to overwrite the "
                "stored results and upload the resulting file")



